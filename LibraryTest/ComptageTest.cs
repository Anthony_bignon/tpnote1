﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using LibraryCompteur;

namespace LibraryTest
{
    [TestClass]
    public class ComptageTest
    {
        [TestMethod]
        public void TestIncrementer()
        {
            Comptage comptons = new Comptage();
            comptons.incrementer();
            Assert.AreEqual(1, comptons.getCompteur());
            comptons.incrementer();
            Assert.AreEqual(2, comptons.getCompteur());
            comptons.incrementer();
            Assert.AreEqual(3, comptons.getCompteur());
        }

        [TestMethod]
        public void TestDecrementer()
        {
            Comptage comptons = new Comptage();
            comptons.decrementer();
            Assert.AreEqual(-1, comptons.getCompteur());
            comptons.decrementer();
            Assert.AreEqual(-2, comptons.getCompteur());
            comptons.decrementer();
            Assert.AreEqual(-3, comptons.getCompteur());
        }

        [TestMethod]
        public void TestGetCompteur()
        {
            Comptage comptons = new Comptage();
            comptons.incrementer();
            Assert.AreEqual(1, comptons.getCompteur());
        }

        [TestMethod]
        public void TestReset()
        {
            Comptage comptons = new Comptage();
            comptons.incrementer();
            comptons.reset();
            Assert.AreEqual(0, comptons.getCompteur());
        }

    }
}
