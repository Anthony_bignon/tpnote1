﻿Imports LibraryCompteur

Public Class frm_BasicCounter
    Dim compteur
    Private Sub frm_BasicCounter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        compteur = New Comptage()
        lbl_compteur.Text = compteur.getCompteur()
    End Sub

    Private Sub btn_incr_Click(sender As Object, e As EventArgs) Handles btn_incr.Click
        compteur.incrementer()
        lbl_compteur.Text = compteur.getCompteur()
    End Sub

    Private Sub btn_decr_Click(sender As Object, e As EventArgs) Handles btn_decr.Click
        compteur.decrementer()
        lbl_compteur.Text = compteur.getCompteur()
    End Sub

    Private Sub btn_raz_Click(sender As Object, e As EventArgs) Handles btn_raz.Click
        compteur.reset()
        lbl_compteur.Text = compteur.getCompteur()
    End Sub
End Class
