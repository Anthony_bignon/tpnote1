﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_total = New System.Windows.Forms.Label()
        Me.btn_decr = New System.Windows.Forms.Button()
        Me.btn_incr = New System.Windows.Forms.Button()
        Me.btn_raz = New System.Windows.Forms.Button()
        Me.lbl_compteur = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbl_total
        '
        Me.lbl_total.AutoSize = True
        Me.lbl_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_total.Location = New System.Drawing.Point(210, 50)
        Me.lbl_total.Name = "lbl_total"
        Me.lbl_total.Size = New System.Drawing.Size(40, 17)
        Me.lbl_total.TabIndex = 0
        Me.lbl_total.Text = "Total"
        '
        'btn_decr
        '
        Me.btn_decr.Location = New System.Drawing.Point(95, 106)
        Me.btn_decr.Name = "btn_decr"
        Me.btn_decr.Size = New System.Drawing.Size(75, 23)
        Me.btn_decr.TabIndex = 1
        Me.btn_decr.Text = "-"
        Me.btn_decr.UseVisualStyleBackColor = True
        '
        'btn_incr
        '
        Me.btn_incr.Location = New System.Drawing.Point(299, 106)
        Me.btn_incr.Name = "btn_incr"
        Me.btn_incr.Size = New System.Drawing.Size(75, 23)
        Me.btn_incr.TabIndex = 2
        Me.btn_incr.Text = "+"
        Me.btn_incr.UseVisualStyleBackColor = True
        '
        'btn_raz
        '
        Me.btn_raz.Location = New System.Drawing.Point(203, 179)
        Me.btn_raz.Name = "btn_raz"
        Me.btn_raz.Size = New System.Drawing.Size(75, 23)
        Me.btn_raz.TabIndex = 3
        Me.btn_raz.Text = "RAZ"
        Me.btn_raz.UseVisualStyleBackColor = True
        '
        'lbl_compteur
        '
        Me.lbl_compteur.AutoSize = True
        Me.lbl_compteur.Location = New System.Drawing.Point(213, 115)
        Me.lbl_compteur.Name = "lbl_compteur"
        Me.lbl_compteur.Size = New System.Drawing.Size(0, 13)
        Me.lbl_compteur.TabIndex = 4
        '
        'frm_BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 262)
        Me.Controls.Add(Me.lbl_compteur)
        Me.Controls.Add(Me.btn_raz)
        Me.Controls.Add(Me.btn_incr)
        Me.Controls.Add(Me.btn_decr)
        Me.Controls.Add(Me.lbl_total)
        Me.MaximumSize = New System.Drawing.Size(500, 300)
        Me.MinimumSize = New System.Drawing.Size(500, 300)
        Me.Name = "frm_BasicCounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_total As Label
    Friend WithEvents btn_decr As Button
    Friend WithEvents btn_incr As Button
    Friend WithEvents btn_raz As Button
    Friend WithEvents lbl_compteur As Label
End Class
