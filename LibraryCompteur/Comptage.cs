﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryCompteur
{
    public class Comptage
    {
        private int compteur;

        public Comptage()
        {
            this.compteur = 0;
        }

        public void incrementer()
        {
            this.compteur++;
        }

        public void decrementer()
        {
            this.compteur--;
        }

        public int getCompteur()
        {
            return this.compteur;
        }

        public void reset()
        {
            this.compteur = 0;
        }
    }
}
